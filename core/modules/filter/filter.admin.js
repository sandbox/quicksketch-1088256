/**
 * @file
 * Attaches administration-specific behavior for the Filter module.
 */

(function ($) {

"use strict";

Drupal.behaviors.filterStatus = {
  attach: function (context, settings) {
    var $context = $(context);
    $context.find('#filters-status-wrapper').once('filter-status', function () {
      $(this).find('input.form-checkbox').bind('click.filterUpdate', function () {
        var $checkbox = $(this);
        var filterId = $checkbox.attr('id').replace(/-status$/, '');
        // Retrieve the tabledrag row belonging to this filter.
        var $row = $(document.getElementById(filterId + '-weight')).closest('tr');
        // Retrieve the vertical tab belonging to this filter.
        var tab = $(document.getElementById(filterId + '-settings')).data('verticalTab');

        if ($checkbox.is(':checked')) {
          $row.show();
          if (tab) {
            tab.tabShow(false).updateSummary();
          }
        }
        else {
          $row.hide();
          if (tab) {
            tab.tabHide().updateSummary();
          }
        }
      });

      // Trigger our bound click handler to update elements to initial state.
      $(this).find('input.form-checkbox').each(function() {
        $(this).triggerHandler('click.filterUpdate');
      });
    });

    // Restripe the filter order table when the tab is activated.
    $context.find('#filter-order').once('filter-restripe', function() {
      var tab = $(this).closest('.vertical-tabs-pane').data('verticalTab');
      $(tab.item).find('a').click(function() {
        Drupal.tableDrag['filter-order'].restripeTable();
      });
    });
  }
};

/**
 * Set summaries for Filter module statuses.
 */
Drupal.behaviors.filterSummaries = {
  attach: function (context) {
    var $context = $(context);
    $context.find('#filters-status-wrapper').drupalSetSummary(function (context) {
      var filterCount = $(context).find('input.form-checkbox:checked').length;
      return filterCount ? Drupal.formatPlural(filterCount, '@count filter enabled', '@count filters enabled') : Drupal.t('No filters enabled');
    });
    $context.find('#edit-filters-filter-html-settings').drupalSetSummary(function (context) {
      var tagCount = $(context).find('#edit-filters-filter-html-settings-allowed-html').val().split('>').length - 1;
      return Drupal.formatPlural(tagCount, '@count tag allowed', '@count tags allowed');
    });
    $context.find('#edit-filters-filter-url-settings').drupalSetSummary(function (context) {
      var charCount = $(context).find('#edit-filters-filter-url-settings-filter-url-length').val();
      return Drupal.t('First @count characters shown', { '@count': charCount });
    });
  }
};

})(jQuery);
