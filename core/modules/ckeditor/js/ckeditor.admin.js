(function ($, Drupal) {

"use strict";

Drupal.ckeditor = Drupal.ckeditor || {};

Drupal.behaviors.ckeditorAdmin = {
  attach: function (context, settings) {
    var $context = $(context);
    $(context).find('.ckeditor-toolbar-configuration').once('ckeditor-toolbar', function() {
      var $wrapper = $(this);
      var $textareaWrapper = $(this).find('.form-item-editor-settings-toolbar-buttons').hide();
      var $textarea = $textareaWrapper.find('textarea');
      var $toolbarAdmin = $(settings.ckeditor.toolbarAdmin);
      var sortableSettings = {
        connectWith: '.ckeditor-buttons',
        placeholder: 'ckeditor-button-placeholder',
        forcePlaceholderSize: true,
        tolerance: 'pointer',
        cursor: 'move',
        stop: adminToolbarValue
      };
      // Add the toolbar to the page.
      $toolbarAdmin.insertAfter($textareaWrapper);

      // Then determine if this is RTL or not.
      var rtl = $toolbarAdmin.css('direction') === 'rtl' ? -1 : 1;
      var $toolbarRows = $toolbarAdmin.find('.ckeditor-buttons');

      // Add the drag and drop functionality.
      $toolbarRows.sortable(sortableSettings);
      $toolbarAdmin.find('.ckeditor-multiple-buttons li').draggable({
        connectToSortable: '.ckeditor-toolbar-active .ckeditor-buttons',
        helper: 'clone'
      });

      // Add keyboard arrow support.
      $toolbarAdmin.on('keyup.ckeditorMoveButton', '.ckeditor-buttons a', adminToolbarMoveButton);
      $toolbarAdmin.on('keyup.ckeditorMoveSeparator', '.ckeditor-multiple-buttons a', adminToolbarMoveSeparator);

      // Add click for help.
      $toolbarAdmin.on('click.ckeditorClickButton', '.ckeditor-buttons a', { type: 'button' }, adminToolbarButtonHelp);
      $toolbarAdmin.on('click.ckeditorClickSeparator', '.ckeditor-multiple-buttons a', { type: 'separator' }, adminToolbarButtonHelp);

      // Add/remove row button functionality.
      $toolbarAdmin.on('click.ckeditorAddRow', 'a.ckeditor-row-add', adminToolbarAddRow);
      $toolbarAdmin.on('click.ckeditorAddRow', 'a.ckeditor-row-remove', adminToolbarRemoveRow);
      if ($toolbarAdmin.find('.ckeditor-toolbar-active ul').length > 1) {
        $toolbarAdmin.find('a.ckeditor-row-remove').hide();
      }

      /**
       * Event callback for keypress. Move buttons based on arrow keys.
       */
      function adminToolbarMoveButton(event) {
        var $button = $(this).parent();
        var $currentRow = $button.closest('.ckeditor-buttons');
        var $destinationRow = null;
        var destinationPosition = $button.index();

        switch (event.keyCode) {
          case 37: // Left arrow.
          case 63234: // Safari left arrow.
            $destinationRow = $currentRow;
            destinationPosition = destinationPosition - (1 * rtl);
            break;
          case 38: // Up arrow.
          case 63232: // Safari up arrow.
          console.log($toolbarRows);
            $destinationRow = $($toolbarRows[$toolbarRows.index($currentRow) - 1]);
            break;
          case 39: // Right arrow.
          case 63235: // Safari right arrow.
            $destinationRow = $currentRow;
            destinationPosition = destinationPosition + (1 * rtl);
            break;
          case 40: // Down arrow.
          case 63233: // Safari down arrow.
            $destinationRow = $($toolbarRows[$toolbarRows.index($currentRow) + 1]);
        }

        if ($destinationRow && $destinationRow.length) {
          // Detach the button from the DOM so its position doesn't interfere.
          $button.detach();
          // Move the button before the button whose position it should occupy.
          var $targetButton = $destinationRow.children(':eq(' + destinationPosition + ')');
          if ($targetButton.length) {
            $targetButton.before($button);
          }
          else {
            $destinationRow.append($button);
          }
          // Update the toolbar value field.
          adminToolbarValue(event, { item: $button });
        }
        event.preventDefault();
      }

      /**
       * Event callback for keyup. Move a separator into the active toolbar.
       */
      function adminToolbarMoveSeparator(event) {
        switch (event.keyCode) {
          case 38: // Up arrow.
          case 63232: // Safari up arrow.
            var $button = $(this).parent().clone().appendTo($toolbarRows.eq(-2));
            adminToolbarValue(event, { item: $button });
            event.preventDefault();
        }
      }

      /**
       * Provide help when a button is clicked on.
       */
      function adminToolbarButtonHelp(event) {
        var $link = $(this);
        var $button = $link.parent();
        var $currentRow = $button.closest('.ckeditor-buttons');
        var enabled = $button.closest('.ckeditor-toolbar-active').length > 0;
        var position = $button.index() + 1; // 1-based index for humans.
        var rowNumber = $toolbarRows.index($currentRow) + 1;
        var type = event.data.type;
        if (enabled) {
          if (type === 'separator') {
            alert(Drupal.t('Separators are used to visually split individual buttons. This "@name" is currently enabled, in row @row and position @position.', { '@name': $link.attr('aria-label'), '@row': rowNumber, '@position': position }) + "\n\n" + Drupal.t('Drag and drop the separator or use the keyboard arrow keys to change the position of this separator.'));
          }
          else {
            alert(Drupal.t('The "@name" button is currently enabled, in row @row and position @position.', { '@name': $link.attr('aria-label'), '@row': rowNumber, '@position': position }) + "\n\n" + Drupal.t('Drag and drop the buttons or use the keyboard arrow keys to change the position of this button.'));
          }
        }
        else {
          if (type === 'separator') {
            alert(Drupal.t('Separators are used to visually split individual buttons. This "@name" is currently disabled.', { '@name': $link.attr('aria-label') }) + "\n\n" + Drupal.t('Drag the button or use the up arrow key to move this separator into the active toolbar. You may add multiple separators to each row.'));
          }
          else {
            alert(Drupal.t('The "@name" button is currently disabled.', { '@name': $link.attr('aria-label') }) + "\n\n" + Drupal.t('Drag the button or use the up arrow key to move this button into the active toolbar.'));
          }
        }
        $link.focus();
        event.preventDefault();
      }

      /**
       * Add a new row of buttons.
       */
      function adminToolbarAddRow(event) {
        var $this = $(this);
        var $rows = $this.closest('.ckeditor-toolbar-active').find('.ckeditor-buttons');
        $rows.last().clone().empty().insertAfter($rows.last()).sortable(sortableSettings);
        $toolbarRows = $toolbarAdmin.find('.ckeditor-buttons');
        $this.siblings('a').show();
        redrawToolbarGradient();
        event.preventDefault();
      }

      /**
       * Remove a row of buttons.
       */
      function adminToolbarRemoveRow(event) {
        var $this = $(this);
        var $rows = $this.closest('.ckeditor-toolbar-active').find('.ckeditor-buttons');
        if ($rows.length === 2) {
          $this.hide();
        }
        if ($rows.length > 1) {
          var $lastRow = $rows.last();
          var $disabledButtons = $wrapper.find('.ckeditor-toolbar-disabled .ckeditor-buttons');
          $lastRow.children(':not(.ckeditor-multiple-button)').prependTo($disabledButtons);
          $lastRow.sortable('destroy').remove();
          $toolbarRows = $toolbarAdmin.find('.ckeditor-buttons');
          redrawToolbarGradient();
        }
        event.preventDefault();
      }

      /**
       * Browser quirk work-around to redraw CSS3 gradients.
       */
      function redrawToolbarGradient() {
        $wrapper.find('.ckeditor-toolbar-active').css('position', 'relative');
        window.setTimeout(function() {
          $wrapper.find('.ckeditor-toolbar-active').css('position', '');
        }, 10);
      }

      /**
       * jQuery Sortable stop event. Save updated toolbar positions to the textarea.
       */
      function adminToolbarValue(event, ui) {
        // Update the toolbar config after updating a sortable.
        var toolbarConfig = [];
        var $button = ui.item;
        $button.find('a').focus();
        $wrapper.find('.ckeditor-toolbar-active ul').each(function() {
          var $rowButtons = $(this).find('li');
          var rowConfig = [];
          if ($rowButtons.length) {
            $rowButtons.each(function() {
              rowConfig.push(this.getAttribute('data-button-name'));
            });
            toolbarConfig.push(rowConfig);
          }
        });
        $textarea.val(JSON.stringify(toolbarConfig, null, '  '));
      }

    });
  },
  detach: function (context, settings) {
    // @todo
  }
};

})(jQuery, Drupal);
